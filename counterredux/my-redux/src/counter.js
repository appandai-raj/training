import { Component } from "react"
import react from "react"
import {connect} from "react-redux"

class Counter extends Component{
    increment = () =>{
        // console.log(this)
        this.props.dispatch({type:"INCREAMENT"})
    }
    decrement = () =>{
        // console.log("decrement")
        this.props.dispatch({type:"DECREMENT"})

    }
    render(){
        return(
        <div>
            <h2>Counter</h2>
            <div>
                <button onClick={this.increment}>+</button>
                <span>{this.props.count }</span>
                <button onClick={this.decrement}>-</button>
            </div>
        </div>
        )
    }
}
function mapStateToProps(state){
    return{
        count:state.count
    }
}
export default connect(mapStateToProps)(Counter)