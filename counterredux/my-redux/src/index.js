import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';


import Counter from './counter';
import {createStore} from "redux"
import Reducer from './reducer';
import {Provider} from "react-redux"



const root = ReactDOM.createRoot(document.getElementById('root'));
const store = createStore(Reducer)
root.render(
  <Provider store={(store)}>
    <Counter />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
