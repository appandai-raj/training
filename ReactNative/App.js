import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Iconant from 'react-native-vector-icons/AntDesign';
import Iconevil from 'react-native-vector-icons/EvilIcons';



export default function App() {
  return (
    <View style={{ flex: 1, backgroundColor: "black", width: "70%", height: "40%" }}>
      <View style={{ flexDirection: "row" }}>
        <Image style={{ height: 130, width: 90, marginTop: "8%", flex: 30 }} source={require('./images/roundimgresize10.png')} />
        <Text style={{ color: "white", flex: 1 }}></Text>
        <View style={{ flexDirection: "column" }}>
          <Text style={{ color: "white", backgroundColor: "blue", padding: "20%",display:"flex",justifyContent:"center",alignItems:"center" }}> <Iconant name="close" color="black" style={{fontSize:20}}/></Text>
          <Text style={{ color: "white", backgroundColor: "#8a2be2", padding: "20%", alignItems: "center", display: "flex", margin: "auto" }}> <Iconant name="sharealt" size={20} color="black"  style={{flex:0.2}}/></Text>
        </View>
      </View>
      <View style={{ flexDirection: "row", backgroundColor: "#7fff00", padding: "5%" }}>
        <View style={{ flex: 2 }}><Text>Siva</Text></View>
        <View style={{ flex: 1 }}><Text><Iconant name="edit" size={20} color="black"  style={{flex:0.2}}/></Text></View>
      </View>
      <View style={{ backgroundColor: "#00ffff", paddingLeft: "15%", paddingTop: "5%", height: "25%" }}>
        <View style={{flexDirection:"row"}}><Iconevil name="location" size={20} color="black"  style={{flex:0.2}}/><Text style={{flex:1}}>Coimbatore,tamilNadu</Text></View>
        <View style={{ paddingTop: "10%",flexDirection:"row" }} >
          <Icon name="user" size={20} color="black"  style={{flex:0.2}}/><Text style={{flex:1}}>Profile</Text>
          {/* <Text><FontAwesome name='trophy' />Profile</Text> */}
        </View>
        <View style={{ paddingTop: "5%",flexDirection:"row" }}><Iconant name="setting" size={20} color="black"  style={{flex:0.2}}/><Text>Setting</Text></View>
        <View style={{ paddingTop: "5%",flexDirection:"row" }}><Iconant name="filter" size={20} color="black"  style={{flex:0.2}}/><Text style={{flex:1}}>Perference</Text></View>
        <View style={{ paddingTop: "5%",flexDirection:"row" }}><Icon name="lock" size={20} color="black"  style={{flex:0.2}}/><Text style={{flex:1}}>Privacy and Safetyyyy</Text></View>
      </View>
      <View style={{ backgroundColor: "#7fff00", paddingLeft: "15%", paddingTop: "5%", height: "25%" }}>
        <View style={{ paddingTop: "10%",flexDirection:"row" }}><Iconant name="gift" size={20} color="black"  style={{flex:0.2}}/><Text style={{flex:1}}>Refer a friends</Text></View>
        <View style={{ paddingTop: "5%",flexDirection:"row" }}><Iconant name="help-with-circle" size={20} color="black"  style={{flex:0.2}}/><Text>Help</Text></View>
        <View style={{ paddingTop: "5%",flexDirection:"row" }}><Iconant name="logout" size={20} color="black"  style={{flex:0.2}}/><Text>Logout</Text></View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
