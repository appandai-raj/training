import React from 'react';
import { Machine } from 'react-xstate-js';


const myMachineConfig = {
    key: 'example1',
    initial: 'A',
    states: {
      A: {
        on: {
          NEXT: 'B',
        },
      },
      B: {
        on: {
          PREVIOUS: 'A',
          NEXT: 'C',
        },
      },
      C: {
        on: {
          PREVIOUS: 'B',
          NEXT: 'D'
        },
      },
      D:{
          on:{
              PREVIOUS: "C",
              NEXT: "Completed"
          }
      },
      Completed:{
          on:{
              PREVIOUS:"D"
          }
      }
    },
  };
 const MyComponent = () => (
  <Machine config={myMachineConfig}>
    {({ service, state }) => (
    <>
     <p>
        
        { }
        {JSON.stringify(state.value)}
      </p>
      <button
        type="button"
        onClick={() => service.send({ type: 'PREVIOUS' })}
      >
        previous
      </button>
      <button
        type="button"
        onClick={() => service.send({ type: 'NEXT' })}
      >
        next
      </button>
     
    </>
    )}
  </Machine>
);
export default MyComponent

