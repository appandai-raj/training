import React, {  useState } from "react"

const FirstComponent = () => {
	return (<h1>A</h1>);
}
const SecondComponent = () => {
	return (<h1>B</h1>);
}
const ThirdComponent = () => {
	return (<h1>C</h1>);
}
const FourthComponent = () => {
	return (<h1>D</h1>);
}
const LoadComponent = (props) => {
	console.log("Component ==> ", props.component)
	switch (props.component) {
		case 1:
			console.log("Case 1")
			return <FirstComponent />;
		case 2:
			console.log("Case 2")
			return <SecondComponent />;
		case 3:
			console.log("Case 3")
			return <ThirdComponent />;
		case 4:
			console.log("Case 4")
			return <FourthComponent />;
	}
}

const MainComponent = () => {
	const [component, setComponent] = useState(1)
	
	const preiviousclickfun = () => {
		setComponent(component - 1);
	}
	const nextclickfun = () => {
		setComponent(component + 1);
	}
	return (
		<div>
			<LoadComponent component={component} />
			<button disabled={component<=1} onClick={preiviousclickfun}>PREVIOUS</button>
			<button disabled={component>=4} onClick={nextclickfun} title={component < 3 ? "NEXT" : "Complete"}>NEXT</button>
		</div>
	)
}
export default MainComponent;